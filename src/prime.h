int is_prime(const int x);
int next_prime(int x);

extern const int PRIME;
extern const int NOT_PRIME;
extern const int UNDEF_PRIME;
