//----- main.c ------

#include <stdio.h>

#include "hash_table.h"

int main(int argc, char** argv){


    //printf("HT_MAX_SIZE: %d\n", HT_MAX_SIZE);
    
    //const char* str = "cat";
    //printf("hash test : %d\n", testHash(str, (int)163, HT_MAX_SIZE));
    
    
    ht_hash_table* ht = ht_new();
    // check size
    printf("-----------------------------\n");
    printf("table sizes \t: %d\t %d\n",  ht->base_size, ht->size );
    printf("-----------------------------\n");

    // fill the hash table
    ht_insert(ht, "DATALOG_SLOW", "archSlow");

    ht_insert(ht, "DATALOG_FAST", "archFast");

    ht_insert(ht, "ALARM_LOG", "archAlarms");

    printf("Table current size: %d\n", ht->count);
    //--- search and printing ---
    printf("-----------------------------\n");
    printf("Key \t\t: Value\n");
    printf("-----------------------------\n");
    
    printf("%s \t: %s\n", "DATALOG_SLOW", ht_search(ht, "DATALOG_SLOW") );
    printf("%s \t: %s\n", "DATALOG_FAST", ht_search(ht, "DATALOG_FAST") );
    printf("%s \t: %s\n", "ALARM_LOG", ht_search(ht, "ALARM_LOG") );
    
    // ---- DELETE Test -----
    ht_insert(ht, "DELKEY", "DelValue");
    
//     printf("-----------------------------\n");
//     printf("Key \t\t: Value\n");
//     printf("-----------------------------\n");
//     
//     printf("%s \t: %s\n", "DATALOG_SLOW", ht_search(ht, "DATALOG_SLOW") );
//     printf("%s \t: %s\n", "DATALOG_FAST", ht_search(ht, "DATALOG_FAST") );
//     printf("%s \t: %s\n", "ALARM_LOG", ht_search(ht, "ALARM_LOG") );
//     printf("%s \t: %s\n", "DELKEY", ht_search(ht, "DELKEY") );
//     printf("Table current size: %d\n", ht->count);
//     
//     printf(" ---- DELETE Test ----- \n");
//    ht_delete(ht, "DELKEY");
//     printf("Table current size: %d\n", ht->count);
// 
//     printf(" ---- UPDATE Test ----- \n");
//     ht_insert(ht, "ALARM_LOG", "archAlarms!!!");
//     printf("Table current size: %d\n", ht->count);
//     printf("%s \t: %s\n", "ALARM_LOG", ht_search(ht, "ALARM_LOG") );
//     
//     printf("%s \t: %s\n", "NON_EXISTING_KEY", ht_search(ht, "NON_EXISTING_KEY") );
//     printf("%s \t: %s\n", "DELKEY", ht_search(ht, "DELKEY") );
// 
//     ht_delete(ht, "DELKEY");
//     printf("%s \t: %s\n", "DELKEY", ht_search(ht, "DELKEY") );
// 
//     printf("-----------------------------\n");
//     printf("table sizes \tbase: %d\t act:%d\n",  ht->base_size, ht->size );
//     printf("-----------------------------\n");
    
//     test_resize(ht, 512);
//     printf("-----------------------------\n");
//     printf("table sizes \tbase: %d\t act:%d\n",  ht->base_size, ht->size );
//     printf("-----------------------------\n");
    
    //--------
    //TODO check ht_entries()
    //---------
    printf(" ---- ht_entries Test ----- \n");
    ht_entries* entries = ht_get_entries(ht);

    printf("KEY\t\t: VALUE\n");
    for (int i = 0; i < entries->size; i++)
    	printf("%s\t: %s\n", entries->items[i]->key
    			, entries->items[i]->value);


    //---------
    
    
    //--- finalizing ---
    ht_del_hash_table(ht);

    return 0;       
    
}
