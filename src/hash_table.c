#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "hash_table.h"
#include "prime.h"

//------
const int HT_INITIAL_BASE_SIZE = 47;
const int HT_PRIME_1 = 151;
const int HT_PRIME_2 = 163;

const int HT_MAX_LOAD = 70;
const int HT_MIN_LOAD = 20;

static ht_item HT_DELETED_ITEM = {NULL, NULL};

//---------------------------
//------- creating ----------
//---------------------------

static ht_item* ht_new_item(const char* k, const char* v){
    
    ht_item* i = malloc(sizeof(ht_item));
    
    i->key = strdup(k);
    i->value = strdup(v);
    //DEBUG
//     printf("--- DEBUG from ht_new_item()\n");
//     printf("new item %s: %s\n", i->key, i->value);
   
    return i;
}
/*
 * Function description
 * */
static ht_hash_table* ht_new_sized(const int base_size){
    // memory allocation for the structure
    ht_hash_table* ht = malloc(sizeof(ht_hash_table));
    
    ht->base_size = base_size;
    ht->size = next_prime(ht->base_size);
    
    ht->count = 0;
        
    ht->items = calloc( (size_t) ht->size, sizeof(ht_item*) );
    
    return ht;
}


ht_hash_table* ht_new(){
    
    return ht_new_sized(HT_INITIAL_BASE_SIZE);
}


//---------------------------
//------- resizing ----------
//---------------------------

static void ht_resize(ht_hash_table* ht, const int base_size){
    //DEBUG
//     printf("DEBUG ht_resize() executed\n");
//     printf("table sizes \tbase: %d\t act:%d\n",  ht->base_size, ht->size );
   
    if (base_size < HT_INITIAL_BASE_SIZE) return;
    
    ht_hash_table* new_ht = ht_new_sized(base_size);
    
    // copy data to new hash hash_table
    for (int i = 0; i < ht->size; i++){
        ht_item* cur_item = ht->items[i];
        if (cur_item != NULL && cur_item != &HT_DELETED_ITEM){
            ht_insert(new_ht, cur_item->key, cur_item->value);
        }
    }
    
    // assign sizes
    ht->base_size = new_ht->base_size;
    ht->count = new_ht->count;
    
    //swap new_ht and ht in order to delete new_ht and deallocate memory
    // sizes
    const int tmp_size = ht->size;
    ht->size = new_ht->size;
    new_ht->size = tmp_size;
    
    // swap items
    ht_item** tmp_items = ht->items;
    ht->items = new_ht->items;
    new_ht->items = tmp_items;
    
    // deleting new_ht
    ht_del_hash_table(new_ht);
}


static void ht_resize_up(ht_hash_table* ht){
    
    const int new_size = ht->base_size * 2;
    
    return ht_resize(ht, new_size);
}


static void ht_resize_down(ht_hash_table* ht){
    
    const int new_size = ht->base_size / 2;
    
    return ht_resize(ht, new_size);
}

// check hastTable load and resize it up or down
static void ht_chk_load_and_resize(ht_hash_table* ht){
    const int ht_load = (ht->count * 100) / ht->size; // in percents
    
    if (ht_load > HT_MAX_LOAD){
        ht_resize_up(ht);
        return;
    }
    
    if (ht_load < HT_MIN_LOAD){
        ht_resize_down(ht);
        return;
    }
}

//---------------------------
//------- deleting ----------
//---------------------------

static void ht_del_item(ht_item* i){
    free(i->key);
    free(i->value);
    free(i);
}


void ht_del_hash_table(ht_hash_table* ht){
    
    for (int i = 0; i < ht->size; i++) {

        ht_item* item = ht->items[i];
        if (item != NULL && item != &HT_DELETED_ITEM)
            ht_del_item(item);
    }
    
    free(ht->items);
    free(ht);
    
}


//---------------------------
//------- hashing -----------
//---------------------------

static int ht_hash(const char* str, const int prime, const int buck_size){

    long hash = 0;
    const int len_str = strlen(str);
    
    for (int i = 0; i < len_str; i++){
        hash += (long)pow(prime, len_str - (i+1)) * str[i];
        hash = hash % buck_size;
    }
            
    return (int)hash;
}


// double hashing to avoid collisions
static int ht_get_hash(const char* str, const int buck_size, 
                       const int nAttempt){
    
    //TODO probably replace HT_PRIME_1 HT_PRIME_2 to calculated max size
    const int hash_a = ht_hash(str, HT_PRIME_1, buck_size);
    const int hash_b = ht_hash(str, HT_PRIME_2, buck_size);
    
    return ( hash_a + nAttempt*(hash_b + 1) ) % buck_size;
    
}



//----------------------------------
//------- Item operations ----------
//----------------------------------

//----- item insert
void ht_insert(ht_hash_table* ht, const char* key, const char* value){
    
    ht_chk_load_and_resize(ht);
    
    ht_item* new_item = ht_new_item(key, value);
    int index = ht_get_hash(new_item->key, ht->size, 0); // try hashing
    ht_item* cur_item = ht->items[index];
    
    //--- check if the bucket is empty (NULL), then insert
    int attempt = 1; // attempt number
    while (cur_item != NULL){

        if (cur_item!= &HT_DELETED_ITEM){
            if (strcmp(cur_item->key, key) == 0){
                ht_del_item(cur_item);
                ht->items[index] = new_item;
                return;
            }           
        }
            
        index = ht_get_hash(new_item->key, ht->size, attempt);
        cur_item = ht->items[index];
        attempt++;
    }
    
    ht->items[index] = new_item;
//     DEBUG
//     printf("--- DEBUG from ht_insert()\n");
//     printf("inserted %d: %s with %d attempt\n", index, new_item->value, attempt);
//     printf("\n");

    ht->count++;
    
}

//----- item search
char* ht_search(ht_hash_table* ht, const char* key){
    
    int index = ht_get_hash(key, ht->size, 0); // try hashing
    ht_item* accs_item = ht->items[index];

    //--- find non-NULL item with the same key. If not succeed,
    //    hash again until NULL or desired key has been found
    int attempt = 1; // attempt number
    while (accs_item != NULL){
        if (accs_item != &HT_DELETED_ITEM)
            if (strcmp(accs_item->key, key) == 0)
                return accs_item->value;
        // hash again in case of collision
        index = ht_get_hash(key, ht->size, attempt);
        accs_item = ht->items[index];
        attempt++;
    }
    // the key isn't found
    return NULL;
} 

//----- item deleting -----
// instead of deletiong from ht_hash_table, we're gonna bind deleted item 
// addess to a special global var pointer


void ht_delete(ht_hash_table* ht, const char* key){
    
    ht_chk_load_and_resize(ht);
    
    int index = ht_get_hash(key, ht->size, 0); // try hashing
    ht_item* del_item = ht->items[index];
    
    // 
    int attempt = 1;
    while (del_item != NULL){
        if (del_item != &HT_DELETED_ITEM)
            if (strcmp(key, del_item->key) == 0){
                ht_del_item(del_item);
                ht->items[index] = &HT_DELETED_ITEM;
                ht->count--;
                return;
            }
        
        index = ht_get_hash(key, ht->size, attempt);
        del_item = ht->items[index];
        attempt++;
    }
    
}

// -------------------------------
// ------- extended API ----------
// -------------------------------

ht_entries* ht_get_entries(const ht_hash_table* ht){
    
    ht_entries* entries = malloc(sizeof(ht_entries));
    entries->items = malloc(sizeof(ht_item));
    
    ht_item* curr_item = malloc(sizeof(ht_item));
    int eInd = 0;

    for (int i = 0; i < ht->size; i++){
        curr_item = ht->items[i];
        if (curr_item != NULL && curr_item != &HT_DELETED_ITEM){

        	entries->items[eInd] = malloc(sizeof(ht_item));
			entries->items[eInd]->key = strdup(curr_item->key);
			entries->items[eInd]->value = strdup(curr_item->value);
            
            entries->size++;
            eInd++;
        }
    }
    
    free(curr_item);
    
    return entries;
}

char** ht_keys(ht_hash_table* ht){
    
    char** keys = calloc( (size_t) ht->count, sizeof(ht_item) ); //SHIT?
    
    ht_item* curr_item = malloc(sizeof(ht_item));

    int iKey = 0;
    for (int i = 0; i < ht->size; i++){
        curr_item = ht->items[i];
        if (curr_item != NULL && curr_item != &HT_DELETED_ITEM){
            keys[iKey] = strdup(curr_item->key);
            iKey++;
        }
    }

    free(curr_item);
    
    return keys;
}

//TODO add ht_values function: return the array of values
// // add dynamic array: int hashes[buck_size]. hashes[i] 
// is added when ht_insert() works. On ht_delete(), delete hashes[i] too.


//TODO add ht_print function: print (key, value) pairs

//---------------------------
//------- testing ----------
//---------------------------

int testHash(const char* str, const int prime, const int buck_size){
    
    return ht_hash(str, prime, buck_size);
    
}

void test_resize(ht_hash_table* ht, const int new_size){
    ht_resize(ht, new_size);
}

void test_print_keys(ht_hash_table* ht){
    
    printf("-----------------------------\n");
    printf("HT Keys\n");
    
    
    printf("-----------------------------\n");    
}
