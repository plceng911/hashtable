#include <math.h>

#include "prime.h"


const int PRIME = 1;
const int NOT_PRIME = 0;
const int UNDEF_PRIME = -1;

/*
 * Return whether x is prime or not
 *
 * Returns:
 *   1  - prime
 *   0  - not prime
 *   -1 - undefined (i.e. x < 2)
 */
int is_prime(const int x){
    
    if (x < 2) return UNDEF_PRIME;
    if (x < 4) return PRIME;
    
    if ((x%2) == 0) return NOT_PRIME;
    
    for (int i = 3; i < floor( sqrt((double)x) ); i += 2)
        if ( (x%i)==0 ) return NOT_PRIME;
    
    return PRIME;
}

int next_prime(int x){
    
    while (is_prime(x) != PRIME) {
        x++;
    }
    
    return x;
}
