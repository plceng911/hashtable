// ----------- hash_table.h ----------
// #include <math.h>

extern const int HT_INITIAL_BASE_SIZE;
extern const int HT_PRIME_1;
extern const int HT_PRIME_2;

extern const int HT_MAX_LOAD;
extern const int HT_MIN_LOAD;

// ht_item: hash table item type
typedef struct ht_item {
    char* key;
    char* value;
} ht_item;

// ht_hash_table: hash table type
typedef struct ht_hash_table {
    int size;
    int count;
    int base_size;
    ht_item** items;
} ht_hash_table;

ht_hash_table* ht_new();
void ht_del_hash_table(ht_hash_table* ht);

void ht_insert(ht_hash_table* ht, const char* key, const char* value);
char* ht_search(ht_hash_table* ht, const char* key);
void ht_delete(ht_hash_table* ht, const char* key);

//------- extended API ----------
typedef struct ht_entries {
    ht_item** items;
    int size;
    
} ht_entries;

char** ht_values(ht_hash_table* ht);
char** ht_keys(ht_hash_table* ht);
ht_entries* ht_get_entries(const ht_hash_table* ht);

//------- tests ----------
int testHash(const char* str, const int prime, const int buck_size);
void test_resize(ht_hash_table* ht, const int new_size);

