#     '%' matches filename.
#     $@: the target filename.
#     $*: the target filename without the file extension.
#     $<: the first prerequisite filename.
#     $^: the filenames of all the prerequisites, separated by spaces, discard duplicates.
#     $+: similar to $^, but includes duplicates.
#     $?: the names of all prerequisites that are newer than the target, 
#     separated by spaces.

SOURCE_DIR=src
HEADER_DIR=src
SRCS=hash_table.c prime.c main.c


BUILD_DIR=build
BIN_DIR=bin
OBJS:=$(addprefix $(BUILD_DIR)/,$(SRCS:.c=.o))
# SRCS:=$(addprefix $(BUILD_DIR)/,$(SRCS))

DBG_DIR=debug
DBGBUILD_DIR=build/debug
DBGCFLAGS= -g -O0 -DDEBUG
DBGOBJS:=$(addprefix $(DBGBUILD_DIR)/,$(SRCS:.c=.o))


# vpath %.c $(SOURCE_DIR)
vpath %.h $(HEADER_DIR)
# vpath %.o $(BUILD_DIR)

#---- General targets ----

clean:
	rm -f $(BUILD_DIR)/*.o
	rm -f $(BIN_DIR)/*
	rm -f $(DBGBUILD_DIR)/*.o
	rm -f $(DBG_DIR)/*
	echo Cleaned

#---- Release targets ----

all: hashTest
	echo All is done!

hashTest: $(OBJS)
# 	echo linking
	gcc -Wall -lm $(OBJS) -o $(BIN_DIR)/hashTest
	echo $(OBJS)

$(BUILD_DIR)/%.o : $(SOURCE_DIR)/%.c
# 	echo compile
	gcc -Wall -c $< -o $@
# 	echo done

#---- Debug targets
all_debug: debugHashTest

debugHashTest: $(DBGOBJS)
	gcc -Wall -lm $(DBGOBJS) -o $(DBG_DIR)/hashTest

$(DBGBUILD_DIR)/%.o : $(SOURCE_DIR)/%.c
	gcc -Wall $(DBGCFLAGS) -c $< -o $@
	
#---- Dependencies ----

$(BUILD_DIR)/hash_table.o : $(HEADER_DIR)/hash_table.h $(HEADER_DIR)/prime.h
$(BUILD_DIR)/main.o: $(HEADER_DIR)/hash_table.h

$(DBGBUILD_DIR)/hash_table.o : $(HEADER_DIR)/hash_table.h $(HEADER_DIR)/prime.h
$(DBGBUILD_DIR)/main.o: $(HEADER_DIR)/hash_table.h
